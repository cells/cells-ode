#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#

(asdf:defsystem :cells-ode
  :name "cells-ode"
  :depends-on (:cells :cl-ode :utils-kt :cffi)
  :serial t
  :components
  ((:file "package")
   (:file "ode-compat")
   (:file "types" :depends-on ("package"))
   (:file "core" :depends-on ("types" "ode-compat"))
   (:file "objects" :depends-on ("core"))
   (:file "mass" :depends-on ("core"))
   (:file "world" :depends-on ("objects"))
   (:file "bodies" :depends-on ("objects"))
   (:file "geoms" :depends-on ("objects"))
   (:file "joints" :depends-on ("objects"))
   (:file "utility" :depends-on ("objects"))
   (:file "primitives" :depends-on ("geoms" "bodies" "mass"))
   (:file "collision" :depends-on ("objects"))
   (:file "simulate" :depends-on ("collision" "objects" "world"))
   (:file "test-c-ode" :depends-on ("simulate"))
   ))