#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


(in-package :c-ode)

;;;
;;; general mass model
;;;

(def-ode-model mass ()
  ((mass-obj :ode nil :cell nil)
   (mass :ode nil :initform (c-in 1))
   (translation :ode nil :initform (c-in #(0 0 0))))
  (:default-initargs
      :mass-obj (foreign-alloc 'ode:mass)))

(defmethod ode-destroy ((self mass))
  (foreign-free (mass-obj self))
  (call-next-method))

(defmethod id ((self mass))
  (mass-obj self))

(defmethod echo-slots append ((self mass))
  '(mass translation))

;;;
;;; set mass methods
;;; 

;;; sphere
(def-ode-method set-sphere-total ((self mass) (mass number) (radius number)))

;;; cylinder with spherical caps
(def-ode-method set-capsule-total ((self mass) (mass number) (direction int) (radius number) (length number)))
; direction:  orientation of long axis (1 = x, 2 = y, 3 = z)

;;; cylinder
(def-ode-method set-cylinder-total ((self mass) (mass number) (direction int) (radius number) (length number)))
; direction:  orientation of long axis (1 = x, 2 = y, 3 = z)

;;; box
(def-ode-method set-box-total ((self mass) (mass number) (length vector)))

;;;
;;; mass utilities
;;;

;;; change mass
(def-ode-method adjust ((self mass) (new-mass number)))

;;; translate (relative to body coordinates)
(def-ode-method translate ((self mass) (displace vector)))

;;; rotate (relative to body coordinates)
;(def-ode-method rotate ((self mass) (rotation matrix)))
;;; TODO:  lack matrix type

;;; add mass to self
(def-ode-method add ((self mass) (mass object)))

;;;
;;; lispy layer
;;;

(defun mass-dir (orientation)
  (1+ (cl:position orientation '(:x :y :z))))

(defobserver translation ((self mass) newval)
  (translate self newval))

;;; sphere mass

(def-ode-model sphere-mass (mass)
  ((radius :ode nil :initform (c-in 1))))

(defobserver mass ((self sphere-mass) newval)
  (set-sphere-total self newval (radius self)))

(defobserver radius ((self sphere-mass) newval)
  (set-sphere-total self (mass self) newval))

(defmethod echo-slots append ((self sphere-mass))
  '(radius))

;;; capsule mass

(def-ode-model capsule-mass (mass)
  ((radius :ode nil :initform (c-in 1))
   (orientation :ode nil :initform (c-in :z))
   (length :ode nil :initform (c-in 1))))

(defobserver mass ((self capsule-mass) newval)
  (set-capsule-total self newval (mass-dir (orientation self)) (radius self) (length self)))

(defobserver radius ((self capsule-mass) newval)
  (set-capsule-total self (mass self) (mass-dir (orientation self)) newval (length self)))

(defobserver orientation ((self capsule-mass) newval)
  (set-capsule-total self (mass self) (mass-dir newval) (radius self) (length self)))

(defobserver length ((self capsule-mass) newval)
  (set-capsule-total self (mass self) (mass-dir (orientation self)) (radius self) newval))

(defmethod echo-slots append ((self capsule-mass))
  '(radius orientation length))

;;; cylinder mass

(def-ode-model cylinder-mass (mass)
  ((radius :ode nil :initform (c-in 1))
   (orientation :ode nil :initform (c-in :z))
   (length :ode nil :initform (c-in 1))))

(defobserver mass ((self cylinder-mass) newval)
  (set-cylinder-total self newval (mass-dir (orientation self)) (radius self) (length self)))

(defobserver radius ((self cylinder-mass) newval)
  (set-cylinder-total self (mass self) (mass-dir (orientation self)) newval (length self)))

(defobserver orientation ((self cylinder-mass) newval)
  (set-cylinder-total self (mass self) (mass-dir newval) (radius self) (length self)))

(defobserver length ((self cylinder-mass) newval)
  (set-cylinder-total self (mass self) (mass-dir (orientation self)) (radius self) newval))

(defmethod echo-slots append ((self cylinder-mass))
  '(radius orientation length))

;;; box mass

(def-ode-model box-mass (mass)
  ((size :ode nil :initform (c-in #(1 1 1)))))

(defobserver mass ((self box-mass) newval)
  (set-box-total self newval (size self)))

(defobserver size ((self box-mass) newval)
  (set-box-total self (mass self) newval))

(defmethod echo-slots append ((self box-mass))
  '(size))