#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


(in-package :c-ode)

(defvar *time* 0)

(defun tst-init ()
  (ode-init)
  (setf *time* 0)
  (make-instance 'geom-plane :params (c-in #(0 0 1 0)) :md-name :plane))

(defun tst-bodies ()
  (tst-init)  
  (make-instance 'body
		 :md-name :body1
		 :position (c-in #(0 -1 1))
		 :mass (c-in (make-instance 'sphere-mass :radius (c-in .5))))
  (make-instance 'body
		 :md-name :body2
		 :position (c-in #(0 1 2))
		 :mass (c-in (make-instance 'capsule-mass :mass (c-in 5) :radius (c-in .5) :length (c-in .5) :orientation (c-in :z))))
  (make-instance 'body
		 :md-name :body3
		 :position (c-in #(0 3 2))
		 :quaternion (c-in #(.7 1 0 0))
		 :mass (c-in (make-instance 'box-mass :mass (c-in 5) :size (c-in #(1 1 1)))))
  
  (setf (mass (mass (obj :body1))) 10)

  (make-instance 'geom-sphere :radius (c-in .5) :md-name :geom1 :body (obj :body1))
  (make-instance 'geom-capsule :radius (c-in .5) :length (c-in .5) :md-name :geom2 :body (obj :body2))
  (make-instance 'geom-box :length (c-in #(1 1 1)) :md-name :geom3 :body (obj :body3))

  (format t "~&~%Use (obj :body[1-3]), (obj :geom[1-3]), or (obj :plane) to query objects.~%")
  *world*)

(defun tst-joints ()
  (tst-init)
  
  (make-instance 'body :md-name :body1 :position (c-in #(10 0 .5)) :mass (make-instance 'sphere-mass :mass 30))
  (make-instance 'geom-box :md-name :geom1 :size #(1 1 1) :body (obj :body1))
  (make-instance 'body :md-name :body2 :position (c-in #(10.6 0 .5)) :mass (make-instance 'sphere-mass :mass .5))
  (make-instance 'geom-box :md-name :geom2 :size #(.1 .5 .1) :body (obj :body2))
  
  (make-instance 'hinge-joint :md-name :joint :axis #(1 0 0) :anchor #(10.5 0.5 .5) :body-1 (obj :body1) :body-2 (obj :body2))
  ;   (attach (obj :joint) (obj :body1) (obj :body2))
  )


(defun tst-run (&key (diag nil) (step-size .01))
  (unless *objects*
    (format t "Use (tst-bodies) or (tst-joints) to set up a test~%Use (tst-init) for a blank world.~%~%"))
  (dotimes (i 50)
    (when (= 0 (mod i 2))
      (format t "~&~,2fs: ~{~&~a: (pos ~a, vel ~a)~#[~:;, ~]~}~%" *time*
	      (loop for name in '(:body1 :body2 :body3 :body4)
		 for body = (obj name)
		 if body collect name
		 and collect (position body)
		 and collect (linear-vel body))))
    (ode-step :diag diag :step-size step-size)
    (incf *time* step-size)))

(defun tst-cleanup ()
  (ode-cleanup))

