#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


;;; this is to correct typos and inconsistencies in cl-ode

(in-package :ode)

(defmacro with-symbol-export (&body body)
  `(progn
     ,@body
     (ukt:eval-now!
      (export ',(mapcar #'second body)))))


;;; joint parameters

(with-symbol-export
 (defconstant +param-f-max-2+ ode:+param-fmax-2+)
 (defconstant +param-f-max-3+ ode:+param-fmax-3+)        
 (defconstant +param-vel+ ode:+paramv-el+)
 (defconstant +param-hi-stop-2+ ode:+param-histop-2+)
 (defconstant +param-lo-stop-2+ 256)
 (defconstant +param-stop-cfm+ ode:+param-topcfm+)
 (defconstant +param-fudge-factor-2+ ode:+param-fudgefactor-2+)
 (defconstant +param-suspension-cfm-2+  ode:+param-suspension-cfm2+) 
 (defconstant +param-suspension-erp-2+ ode:+param-suspension-erp2+)) 


;;; contact

(with-symbol-export
 (defconstant +contact-bounce+ ode:+contact-bounce))

;;; hinge2

(with-symbol-export
 (define-symbol-macro joint-set-hinge2-axis1 ode:joint-set-hinge2axis1)
 (define-symbol-macro joint-set-hinge2-axis2 ode:joint-set-hinge2axis2)
 (define-symbol-macro joint-set-hinge2-param ode:joint-set-hinge2param)
 (define-symbol-macro joint-set-hinge2-anchor ode:joint-set-hinge2anchor)

 (define-symbol-macro joint-get-hinge2-anchor2 ode:joint-get-hinge2anchor2)
 (define-symbol-macro joint-get-hinge2-axis1 ode:joint-get-hinge2axis1) 
 (define-symbol-macro joint-get-hinge2-param ode:joint-get-hinge2param)
 (define-symbol-macro joint-get-hinge2-anchor ode:joint-get-hinge2anchor)
 (define-symbol-macro joint-get-hinge2-angle1 ode:joint-get-hinge2angle1) 
 (define-symbol-macro joint-get-hinge2-angle1rate ode:joint-get-hinge2angle1rate)
 (define-symbol-macro joint-get-hinge2-angle2rate ode:joint-get-hinge2angle2rate)) 
