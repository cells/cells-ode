#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


;;;
;;; geom
;;;

(in-package :c-ode)


(defvar *space* (null-pointer) "ODE space for collision detection")

(def-ode-model (general-geom :ode-class geom) (collideable-object)
  ((geom-obj :ode nil :cell nil)
   (space :type object :read-only t)
   (geom-class :type int :ode-slot class :read-only t)  	; one of ode:+...-class+

   (body :ode nil :cell nil :initform *environment*)
   ;   (category-bits)  ; TODO -- missing ulong type
   ;   (collide-bits)   ; TODO -- missing ulong type

   ))

(export! ode-space)
(defmethod ode-space ((self general-geom))
  (^space))

(defmethod ode-destroy ((self general-geom))
  (call-ode geom-destroy ((self object)))
  (call-next-method))

(defmethod id ((self general-geom))
  (geom-obj self))

(defmethod echo-slots append ((self general-geom))
  '())

(def-ode-method is-space ((self general-geom geom)) bool)

(def-ode-method enable ((self general-geom geom)))
(def-ode-method disable ((self general-geom geom)))
(def-ode-method is-enabled ((self general-geom geom)) bool)

;;;
;;; placable geoms
;;; 

(def-ode-model geom (general-geom)
  ((body :type object)
   (position :type vector)
   (quaternion :type quaternion :result-arg t)
   ))

(export! ode-position)
(defmethod ode-position ((self geom))
  (^position))

(defmethod echo-slots append ((self geom))
  '(position quaternion))

;;; sphere

(def-ode-model geom-sphere (geom)
  ((radius :type number :initform (c-in 1) :auto-update nil))
  (:default-initargs
      :geom-obj (call-ode create-sphere ((*space* object) (1 number)))))

(def-ode-method point-depth ((self geom-sphere) (point vector)) number)   ; inside is positive

(defmethod echo-slots append ((self geom-sphere))
  '(radius))

;;; box

(def-ode-model geom-box (geom)
  ((lengths :type vector :initform (c-in #(1 1 1)) :auto-update nil))
  (:default-initargs
      :geom-obj (call-ode create-box ((*space* object) (#(1 1 1) vector)))))

(def-ode-method point-depth ((self geom-box) (point vector)) number)   ; inside is positive

(defmethod echo-slots append ((self geom-box))
  '(lengths))

;;; capsule (aligned along Z axis)

(def-ode-model geom-capsule (geom)
  ((radius :ode nil)
   (length :ode nil)
   ) 
  (:default-initargs
      :geom-obj (call-ode create-capsule ((*space* object) (1 number) (1 number)))))

(export! ode-length)
(defmethod ode-length ((self geom-capsule))
  (^length))

(def-ode-method set-params ((self geom-capsule) (radius number) (length number)))

(defobserver radius ((self geom-capsule) newval)
  (when newval
    (set-params self newval (length self))))

(defobserver length ((self geom-capsule) newval)
  (when newval
    (set-params self (radius self) newval)))

(def-ode-method point-depth ((self geom-capsule) (point vector)) number)   ; inside is positive

(defmethod echo-slots append ((self geom-capsule))
  '(radius length))

;;;
;;; non-placeable geoms
;;;

;;; plane

(def-ode-model geom-plane (general-geom)
  ((params :type vector-4 :initform (c-in #(0 0 1 0)) :result-arg t :auto-update nil) 	; coefficients of ax+by+cz=d
   ) 
  (:default-initargs
      :geom-obj (call-ode create-plane ((*space* object) (#(0 0 1 0) vector-4)))))

(def-ode-method point-depth ((self geom-plane) (point vector)) number)   ; inside is positive

(defmethod echo-slots append ((self geom-plane))
  '())


;;; ray

(def-ode-model geom-ray (general-geom)
  ((length :type number :initform (c-in 1))
   (starting-point :ode nil)
   (direction :ode nil)) 
  (:default-initargs
      :geom-obj (call-ode create-ray ((*space* object) (1 number)))))

(export! ode-length)
(defmethod ode-length ((self geom-ray))
  (^length))

(def-ode-method (ray-set :ode-name set) ((self geom-ray) (starting-point vector) (direction vector)))

(defobserver starting-point ((self geom-ray) newval)
  (when newval (ray-set self newval (length self))))

(defobserver direction ((self geom-ray) newval)
  (when newval (ray-set self (direction self) newval)))

(defmethod echo-slots append ((self geom-ray))
  '(length starting-point direction))




;;; TODO: triangle mesh class

;;;
;;; Geom transform
;;;

;;; create a geom, position it relative to the origin, remove it from the space
;;; then attach the transform to it and use the transform instead of the geom

(def-ode-model geom-transform ()
  ((geom :type object)
   (cleanup :type bool)
   (mode :type bool :auto-update nil :initform (c-in t)))   ; t --> the transform is reported to collide, can use (body geom)
  (:default-initargs
      :ode-id (call-ode create-geom-transform ((*space* object)))))

(defmethod echo-slots append ((self geom-transform))
  '(geom cleanup mode))









