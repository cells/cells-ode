#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


(in-package :c-ode)

;;;
;;; body
;;;


(def-ode-model body ()
  ((position :type vector)
   (linear-vel :type vector)
   (angular-vel :type vector)
   (quaternion :type quaternion)

   (force :type vector)
   (torque :type vector)

   (mass :type mass :result-arg t)
   
   (auto-disable-flag :type bool)
   (auto-disable-linear-threshold)
   (auto-disable-angular-threshold)
   (auto-disable-steps :type int)
   (auto-disable-time)
   
   (finite-rotation-mode :type bool) ; 0 = infinitesimal, 1 = finite
   (finite-rotation-axis :type vector :result-arg t)

   (gravity-mode :type bool :initform (c-in t)))
  (:default-initargs
      :ode-id (call-ode body-create ((*world* object)))))

(export! ode-position)
(defmethod ode-position ((self body))
  (^position))

(defmethod initialize-instance :after ((self body) &rest initargs))

(defmethod ode-destroy ((self body))
  (call-ode body-destroy ((self object)))
  (call-next-method))


(defmethod echo-slots append ((self body))
  '(position linear-vel angular-vel quaternion))


;;;
;;; Forces
;;;

;;; add force or torque

(def-ode-method add-force ((self body) (force vector)))
(def-ode-method add-torque ((self body) (force vector)))
(def-ode-method add-rel-force ((self body) (force vector)))
(def-ode-method add-rel-torque ((self body) (force vector)))


;;; add force at a point

(def-ode-method add-force-at-pos ((self body) (force vector) (pos vector)))
(def-ode-method add-force-at-rel-pos ((self body) (force vector) (pos vector)))
(def-ode-method add-rel-force-at-pos ((self body) (force vector) (pos vector)))
(def-ode-method add-rel-force-at-rel-pos ((self body) (force vector) (pos vector)))

;;;
;;; coordinate transforms
;;; 

;;; get absolute velocity or position for a point

(def-ode-method get-rel-point-pos ((self body) (point vector) (result vector)))
(def-ode-method get-rel-point-vel ((self body) (point vector) (result vector)))
(def-ode-method get-point-vel ((self body) (point vector) (result vector)))

;;; get relative position for a point

(def-ode-method get-pos-rel-point ((self body) (point vector) (result vector)))

;;; rotate a vector to/from relative coordinates

(def-ode-method vector-to-world ((self body) (point vector) (result vector)))
(def-ode-method vector-from-world ((self body) (point vector) (result vector)))


;;;
;;; auto disabling
;;;

(def-ode-method enable ((self body)))
(def-ode-method disable ((self body)))
(def-ode-method is-enabled ((self body)) bool)
(def-ode-method set-auto-disable-defaults ((self body)))

;;;
;;; Joint handling
;;;

(def-ode-method get-num-joints ((self body)) number)
(def-ode-method get-joint ((self body) (index int)) object)





