#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


(in-package :c-ode)

;;;
;;; joint
;;;

(def-ode-model joint ()
  ((joint-type :type int :ode-slot type  :read-only t) ; returns one constant +ode:joint-type-...+
   (feedback-struct :ode nil :cell nil :initform (foreign-alloc 'ode:joint-feedback))
   (body-1 :ode nil)
   (body-2 :ode nil)
   (force-1 :ode nil)
   (torque-1 :ode nil)
   (force-2 :ode nil)
   (torque-2 :ode nil))
  (:default-initargs :ode-id (error "You must instantiate a subtype of joint!")))

(defmethod initialize-instance :after ((self joint) &rest initargs)
  (call-ode joint-set-feedback ((self object) ((feedback-struct self)))))

(defmethod ode-destroy ((self joint))
  (call-ode joint-destroy ((self object)))
  (foreign-free (feedback-struct self))
  (call-next-method))

(defmacro propagate-feedback (feedback-struct joint)
  `(with-foreign-slots ((ode:f-1 ode:t-1 ode:f-2 ode:t-2) ,feedback-struct ode:joint-feedback)
     ,@(loop for (ode slot) on '(f-1 force-1 t-1 torque-1 f-2 force-2 t-2 torque-2) by #'cddr
	  collect `(setf (,slot ,joint) (coerce (loop for i from 0 below 3 collecting (mem-aref ,(intern (string ode) :ode) 'real i)) 'vector)))))

(defmethod update :after ((self joint))
  (unless (typep self 'contact-joint)
   (propagate-feedback (feedback-struct self) self)))


;;;
;;; joint types
;;;

(def-ode-model (ball-joint :ode-class joint :ode-joint ball :joint-axes 0) (joint)
  ((anchor :type vector :result-arg t :auto-update nil)
   (anchor2 :type vector :result-arg t :read-only t))
  (:default-initargs :ode-id (call-ode joint-create-ball ((*world* object) ((null-pointer))))))


(def-ode-model (hinge-joint :ode-class joint :ode-joint hinge :joint-axes 1) (joint)
  ((anchor :type vector :result-arg t :auto-update nil)
   (axis :type vector :result-arg t :auto-update nil)   
   (anchor2 :type vector :result-arg t :read-only t)
   (angle :type number :read-only t)
   (angle-rate :type number :read-only t))
  (:default-initargs :ode-id (call-ode joint-create-hinge ((*world* object) ((null-pointer))))))

#+slider-fixed
(def-ode-model (slider-joint :ode-class joint :ode-joint slider :joint-axes 2) (joint)
  ((axis :type vector :result-arg t :auto-update nil)
   (position :type number :read-only t)
   (positionrate :type number :read-only t))
  (:default-initargs :ode-id (call-ode joint-create-slider ((*world* object) ((null-pointer))))))

(def-ode-model (universal-joint :ode-class joint :ode-joint universal :joint-axes 2) (joint)
  ((anchor :type vector :result-arg t :auto-update nil)
   (axis1 :type vector :result-arg t :auto-update nil)
   (axis2 :type vector :result-arg t :auto-update nil)
   (anchor2 :type vector :result-arg t :read-only t)
   (angle1 :type number :read-only t)
   (angle1rate :type number :read-only t)
   (angle2 :type number :read-only t)
   (angle2rate :type number :read-only t))
  (:default-initargs :ode-id (call-ode joint-create-universal ((*world* object) ((null-pointer))))))

(def-ode-model (hinge-2-joint :ode-class joint :ode-joint hinge2 :joint-axes 2) (joint)
  ((anchor :type vector :result-arg t :auto-update nil)
   (axis1 :type vector :result-arg t :auto-update nil)
   (axis2 :type vector :result-arg t :auto-update nil)
   (anchor2 :type vector :result-arg t :read-only t)
   (angle1 :type number :read-only t)
   (anglerate1 :type number :read-only t)
   (angle2 :type number :read-only t)
   (anglerate2 :type number :read-only t))
  (:default-initargs :ode-id (call-ode joint-create-hinge2 ((*world* object) ((null-pointer))))))

#+a-motor-fixed
(progn
 (defmodel a-motor-axis ()
   ((axis :initarg :axis :accessor axis :initform (c-in #(1 0 0)))
    (angle :initarg :angle :accessor angle :initform (c-in 0))
    (relative-to :initarg :relative-to :accessor relative-to :initform (c-in :body1))
    #+future-ode (rate :initarg :rate :accessor :rate :initform (c-in 0))
    (num :initarg :num :reader num)
    (owner :initarg :owner :initform (error "need to supply :owner for a-motor-axis") :reader owner)))
 
 (def-ode-model (a-motor-joint :ode-class joint :ode-joint a-motor :joint-axes 2) (joint)
   ((mode :type int :auto-update nil :initform (c-in ode:+a-motor-user+)) ; ode:+a-motor-user+ or ode:+a-motor-euler+
    (num-axes :type int :auto-update nil :initform (c-in 0))
    (axes :ode nil :initform (c? (coerce
				  (let (res)
				    (dotimes (i (^num-axes) res)
				      (push (make-instance 'a-motor-axis
							   :owner self
							   :num i) res))
				    (nreverse res)) 'vector)))) ; a vector of num-axes a-motor-axis models
   (:default-initargs :ode-id (call-ode joint-create-a-motor ((*world* object) ((null-pointer)))))))

;;;
;;; contact joint
;;;

(def-ode-model (contact-joint :ode-class joint :ode-joint contact) (joint)
  ()
  (:default-initargs :ode-id (error "Use mk-contact-joint to create a contact joint")))

(defun mk-contact-joint (joint-group contact &rest initargs)
  (let ((joint (apply #'make-instance 'contact-joint :ode-id (call-ode joint-create-contact ((*world* object) (joint-group object) contact)) initargs)))
    (push joint (joints joint-group))
    joint))

;;; 
;;; Attaching
;;;

(def-ode-method attach ((self joint) (body1 object) (body2 object)))
(def-ode-method set-fixed ((self joint)))
(def-ode-method get-body ((self joint) (index int)) object)

(defobserver body-1 ((self joint))
  (when (and new-value (^body-2))
    (attach self new-value (^body-2))))

(defobserver body-2 ((self joint))
  (when (and new-value (^body-1))
    (attach self (^body-1) new-value)))

(defmethod bodies ((self joint))
  (list (get-body self 0) (get-body self 1)))


(def-ode-fun are-connected ((body1 object) (body2 object)) bool)
(def-ode-fun are-connected-excluding ((body1 object) (body2 object) (joint-type int)) bool)

;;; AMotor stuff

#+a-motor-fixed
(progn
 (define-constant +a-motor-axis-rel+ '(:global :body1 :body2))

 (def-ode-method set-a-motor-axis ((self a-motor-joint joint) (axis-num int) (relative-to int) (axis vector))
   nil
   (let ((relative-to (or (cl:position relative-to +a-motor-axis-rel+)
			  (error "axis-X-rel has to be one of :global, :body1, :body2 (and not ~a)" relative-to))))
     (call-ode-method)))

 (def-ode-method get-a-motor-axis ((self a-motor-joint joint) (axis-num int) (result vector)))
 (def-ode-method get-a-motor-axis-rel ((self a-motor-joint joint) (axis-num int))
   int
   (nth (call-ode-method) +a-motor-axis-rel+))

 (def-ode-method set-a-motor-angle ((self a-motor-joint joint) (axis-num int) (angle number)))
 (def-ode-method get-a-motor-angle ((self a-motor-joint joint) (axis-num int)) number)

 #+future-ode (def-ode-method get-a-motor-angle-rate ((self a-motor-joint joint) (axis-num int)) number)
;;; PH 02.2008 -- this is not supported in ODE 0.8

;;; AMotor cellified

 (defobserver axis ((self a-motor-axis) newval)
   (when newval
     (set-a-motor-axis (owner self) (num self) (relative-to self) newval)))

 (defobserver relative-to ((self a-motor-axis) newval)
   (when newval
     (set-a-motor-axis (owner self) (num self) newval (axis self))))

 (defobserver angle ((self a-motor-axis) newval)
   (when newval
     (set-a-motor-angle (owner self) (num self)  newval)))

 (defmethod update :after ((self a-motor-joint))
   (loop for num from 0 below (num-axes self)
      do (with-accessors ((axis axis) (angle angle) #+future-ode (rate rate))
	     (aref (axes self) num)
	   (setf axis (get-a-motor-axis self num)
		 angle (get-a-motor-angle self num))
	   #+future-ode (setf rate (get-a-motor-angle-rate self num))))))

;;; TODO:  Add Torque directly

;;;
;;; joint-group
;;;

(def-ode-model joint-group ()
  ((joints :ode nil :initform (c-in nil)))
  (:default-initargs
      :ode-id (error "use mk-joint-group to create a joint-group")))

(defun mk-joint-group (max-size &rest initargs)
  (apply #'make-instance 'joint-group :ode-id (call-ode joint-group-create ((max-size int))) initargs))

(defmethod ode-destroy ((self joint-group))
  (empty self)
  (call-ode joint-group-destroy ((self object)))
  (call-next-method))

(def-ode-method empty ((self joint-group))
  nil
  (dolist (joint (joints self)) (ode-destroy joint ))
  (call-ode-method))
