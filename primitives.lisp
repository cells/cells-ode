#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


;;;
;;; code to implement primitives (body + mass + geom)
;;; 

(in-package :c-ode)
;;;
;;; different body shapes
;;; 

(defmacro def-ode-prim (name &key mass-setter-args)
  `(progn
     (def-ode-model (,name  :ode-class body) (body)
       ((mass :ode nil)))
     (defobserver mass ((self ,name) newval)
       (when newval
	 (call-ode ,(intern-string 'mass-set name 'total) (((mass-obj self) object) (newval number) ,@mass-setter-args))))))

(def-ode-prim sphere
    :mass-setter-args (((radius self) number)))

(def-ode-prim capped-cylinder
    :mass-setter-args ((())))